<?php 
namespace Drupal\alt_book_nav\Plugin\Block;
use Drupal\book\Plugin\Block\BookNavigationBlock;
use Drupal\book\BookManager;
//use Drupal\book\BookOutline;


  /**
   * Provides a 'Book navigation' block.
   *
   * @Block(
   *   id = "custom_book_navigation",
   *   admin_label = @Translation("Book navigation - Customized"),
   *   category = @Translation("Menus")
   * )
   */
class CustomBookNavigationBlock extends BookNavigationBlock {

  /**
   * {@inheritdoc}
   */

  public function check_links($tree_array, $page_nid){

    $nested_array=[];

    foreach($tree_array as $row_name=>$row_content){ // 50000=>array

      foreach($row_content as $structure=>$content){ // link=>array, below=>array
        if($structure == "link"){
          foreach($content as $key=>$value){ // 
            if($key != "in_active_trail") $nested_array[$row_name][$structure][$key]=$value;
            if($key == "nid"){
              if($value == $page_nid){
                $nested_array[$row_name][$structure][$key] = $value;
                $nested_array[$row_name][$structure]["in_active_trail"] = true;
              } else { $nested_array[$row_name][$structure]["in_active_trail"] = false; }
            } 
          }
          
        } else if ($structure == "below"){
          $nested_array[$row_name][$structure] = $this->check_links($content, $page_nid);
        }

      }
    }

    return $nested_array;
  }
  
   
  public function build() {
    $current_bid = 0;
      // ksm($this);

    if ($node = $this->requestStack->getCurrentRequest()->get('node')) {
      $current_bid = empty($node->book['bid']) ? 0 : $node->book['bid'];
      $current_page_id = $node->book['nid'];
    }
    if ($this->configuration['block_mode'] == 'all pages') {
      return parent::build();
    }
    elseif ($current_bid) {
      // Only display this block when the user is browsing a book and do
      // not show unpublished books.
      $nid = \Drupal::entityQuery('node')
        ->condition('nid', $node->book['bid'], '=')
        ->condition('status', 1) //->condition('status', NODE_PUBLISHED)
        ->execute();

      // Only show the block if the user has view access for the top-level node.
      if ($nid) {
        // We want to show the whole tree, by just removing '$node->book'
        // $tree = $this->bookManager->bookTreeAllData($node->book['bid'], );
        
        $tree = $this->bookManager->bookTreeAllData($node->book['bid']);

        $modTree = $this->check_links($tree, $current_page_id);
        
        $tree = $this->bookManager->bookTreeOutput($modTree);
        
        $tree['#theme']='book_tree_custom';

        return $tree;

        }
      }
    return array();
  }

}